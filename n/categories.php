<!DOCTYPE html>
<html>
<title>categories</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="styles.css">
<body></body>
<div class="w3-container">

        <h1>NutriPlant</h1>
        <div class="Image">
                <img src="logo.png" class="align center medium"/>
                </div>
               
<button onclick="myFunction('Demo1')" class="w3-button w3-block w3-black w3-left-align">Rainy Season</button>
<div id="Demo1" class="w3-hide w3-container">
    <h3>Some text..</h3>
</div>

<button onclick="myFunction('Demo2')" class="w3-button w3-block w3-black w3-left-align">Dry Season</button>
<div id="Demo2" class="w3-hide w3-container">
  <p><a href="#" >Asparagus</a></p>
    
</div>


</div>
<script>
function myFunction(id) {
    var x = document.getElementById(id);
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
        x.previousElementSibling.className = 
        x.previousElementSibling.className.replace("w3-blank", "w3-red");
    } else { 
        x.className = x.className.replace(" w3-show", "");
        x.previousElementSibling.className = 
        x.previousElementSibling.className.replace("w3-red", "w3-black");
    }
}
</script>

</body>
</html>
